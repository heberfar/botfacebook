//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');

var async = require('async');
var socketio = require('socket.io');
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var fs = require('fs');

var now = new Date();
var ano = now.getFullYear();
var mes = now.getMonth()+1; //January is 0!

var per1 = mes.toString() + "/" + ano.toString();

mes = mes - 1;
if (mes === 0){  mes = 12; ano = ano - 1;	};
var per2 = mes.toString() + "/" + ano.toString();

mes = mes - 1;
if (mes === 0){  mes = 12; ano = ano - 1;	};
var per3 = mes.toString() + "/" + ano.toString();

mes = mes - 1;
if (mes === 0){  mes = 12; ano = ano - 1;	};
var per4 = mes.toString() + "/" + ano.toString();

mes = mes - 1;
if (mes === 0){  mes = 12; ano = ano - 1;	};
var per5 = mes.toString() + "/" + ano.toString();

mes = mes - 1;
if (mes === 0){  mes = 12; ano = ano - 1;	};
var per6 = mes.toString() + "/" + ano.toString();

//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false}));

var messages = [];
var sockets = [];


router.get('/webhook', function(req, res){
  console.log(req.query['rub.mode']);
  console.log(req.query['hub.verify_token']);
  
  if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === 'minhasenha123'){
    console.log('Validação ok!');
    res.status(200).send(req.query['hub.challenge']);
  }else{
    //console.log('Validação falhou!');
    res.sendStatus(403);
  };
  
});

router.post('/webhook', function(req, res){
  //var nome = event.recipient.
  var data = req.body;
  
  if (data && data.object === 'page'){
  
    //percorrer todas as entradas
      data.entry.forEach(function(entry){
      var pageID = entry.id;
      var timeOfEvent = entry.time;
  
        //percorrer todas as mensagens
      entry.messaging.forEach(function (event) {
        if (event.message){
          trataMensagem(event);
        }else{
          if(event.postback && event.postback.payload){
            //console.log('chamos um payload ',event.postback.payload);
            switch (event.postback.payload) {
              case 'clicou_comecar':
                // code
                //sendTextMessage(event.sender.id,'Como posso te ajudar? Selecione uma das opções abaixo:');
                sendFirstMenu(event.sender.id);
                break;
              case 'clicou_holerite':
                // code
                sendHoleriteMenu(event.sender.id);
                break;
                
              default:
                // code
                if (event.postback.payload === per1 || event.postback.payload === per2 || event.postback.payload === per3 || event.postback.payload === per4 || event.postback.payload === per5 || event.postback.payload === per6) {
                  sendTextMessage(event.sender.id,'Ok, estou gerando seu holerite ' + event.postback.payload + ', assim que estiver pronto anexo ele aqui para voce. ');
                  // chamada do serviço de criação do arquivo enviado os parametros event.sender.id e event.postback.payload
                  sendFileMessage(event.sender.id,"https://s3.us-east-2.amazonaws.com/storageface/Anota%C3%A7%C3%A3o.txt");
                }
                
            }
          }
        }
      })
    })
    
     res.sendStatus(200);
    
  };
  
});


function trataMensagem(event){
  
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfEvent = event.timestamp;
  var message = event.message;
  
  //console.log('mensagem recebida do usuário %d pela página %d', senderID, recipientID);
  var messageID = message.mid;
  var messageText = message.text;
  var files = message.attachments;
  
  if (messageText){
    
    var texto = messageText.toUpperCase();
    
    if (texto.indexOf('OI') != -1 || texto.indexOf('OLÁ') != -1 || texto.indexOf('OLA') != -1 || texto.indexOf('BOM DIA') != -1 || texto.indexOf('BOA TARDE') != -1 || texto.indexOf('BOA NOITE') != -1){
      
      sendTextMessage(senderID,'Olá, sou seu auxiliar para serviços do RH, por aqui voce conseguirá solicitar seu holerite entre outras coisas, vamos começar !');
      sendFirstMenu(event.sender.id); 
      
    }else if (texto.indexOf('HOLERITE') != -1){
      
      sendHoleriteMenu(event.sender.id);
        
    } else if(texto.indexOf('TCHAU') != -1) {
      
      sendTextMessage(senderID,'Tchau, volte sempre.');

    }else{
      
      sendTextMessage(senderID,'Não consegui entender, vamos tentar novamente.');
      sendFirstMenu(event.sender.id); 
      
    }

  }else if(files){
    console.log('tem anexos');
  }
}

function sendTextMessage(recipientid, messageText){
  var messageData = {
    recipient: {
      id: recipientid
    },
    message: {
      text: messageText
    }
  };
  
  callSendAPI(messageData);
}

function sendFileMessage(recipientid, urlText){
  var messageData = {
    recipient: {
      id: recipientid
    },
    message: {
      attachment:{
        type:"file",
        payload:{
        url:urlText
        }
      }
    }
  };
  
  callSendAPI(messageData);
}

function sendFirstMenu(recipientid){
    var messageData = {
    recipient: {
      id: recipientid
    },
    message: {
      attachment:{
        type: 'template',
        payload:{
          template_type:'button',
          text:'Como posso te ajudar? Selecione uma das opções abaixo:',
          buttons:[
  //          {
  //            type:'web_url',
  //            url:'https://www.google.com',
  //            title:"Acessar o ALTSA"
  //          },
            {
              type:'postback',
              title:"Holerite",
              payload:'clicou_holerite'
            }
  //          {
  //            type:'postback',
  //            title:"Alteração data de férias",
  //            payload:'clicou_ferias'
  //          }
            ]
        }
      }
    }
  };
  
  callSendAPI(messageData);
}


function sendHoleriteMenu(recipientid){
/*    var now = new Date();
    var ano = now.getFullYear();
    var mes = now.getMonth()+1; //January is 0!

  	var per1 = mes.toString() + "/" + ano.toString();
  	
  	mes = mes - 1;
  	if (mes === 0){  mes = 12; ano = ano - 1;	};
  	var per2 = mes.toString() + "/" + ano.toString();
  	
  	mes = mes - 1;
  	if (mes === 0){  mes = 12; ano = ano - 1;	};
  	var per3 = mes.toString() + "/" + ano.toString();
  	
  	mes = mes - 1;
  	if (mes === 0){  mes = 12; ano = ano - 1;	};
  	var per4 = mes.toString() + "/" + ano.toString();
  	
  	mes = mes - 1;
  	if (mes === 0){  mes = 12; ano = ano - 1;	};
  	var per5 = mes.toString() + "/" + ano.toString();
  	
  	mes = mes - 1;
  	if (mes === 0){  mes = 12; ano = ano - 1;	};
  	var per6 = mes.toString() + "/" + ano.toString();
*/

    var messageData = {
    recipient: {
      id: recipientid
    },
    message: {
      attachment:{
        type: 'template',
        payload:{
          template_type:'button',
          text:'Entendi, para gerar seu holerite vou precisar que voce me informe o mes e ano que deseja gerar:',
          buttons:[
            {
              type:'postback',
              title:per1 ,
              payload:per1 //'clicou_holerite1'
            },
            {
              type:'postback',
              title:per2 ,
              payload:per2 //'clicou_holerite2'
            },
            {
              type:'postback',
              title:per3 ,
              payload:per3 //'clicou_holerite3'
            }
            ]
        }
      }
    }
  };
  
  callSendAPI(messageData);
  
  messageData = {
    recipient: {
      id: recipientid
    },
    message: {
      attachment:{
        type: 'template',
        payload:{
          template_type:'button',
          text:'Mais...',
          buttons:[
            {
              type:'postback',
              title:per4 ,
              payload:per4 //'clicou_holerite4'
            },
            {
              type:'postback',
              title:per5 ,
              payload:per5 //'clicou_holerite5'
            },
            {
              type:'postback',
              title:per6 ,
              payload:per6 //'clicou_holerite6'
            }
            ]
        }
      }
    }
  };
  setTimeout(function(){
    callSendAPI(messageData);  
  },1000);
  
}  

function callSendAPI (messageData){
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:'EAAB4935EAsgBADbBslF2K0mWAtKfgPA3csAnZAO0PBwm5GCpD6iC1X5cTjggo7iqa8UH2KfWETt61auQXW5bdVkBnwYn392ZBMziDW1lVE044lB2n2J7ee0hM7Qb77YswvqVr38lowbTMP7HqAA42z0UinXGqJZBOJP1300RQZDZD'},
    method: 'POST',
    json: messageData
  }, function(error, response, body){
    if(!error && response.statusCode == 200){
      //console.log('mensagem enviada com sucesso');
      var recipientID = body.recipient_id;
      var messageID = body.message_id;
    }else{
      console.log('não foi possível enviar a mensagem!');
      console.log(error);
    }
  })
}











io.on('connection', function (socket) {
    messages.forEach(function (data) {
      socket.emit('message', data);
    });

    sockets.push(socket);

    socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      updateRoster();
    });

    socket.on('message', function (msg) {
      var text = String(msg || '');

      if (!text)
        return;

      socket.get('name', function (err, name) {
        var data = {
          name: name,
          text: text
        };

        broadcast('message', data);
        messages.push(data);
      });
    });

    socket.on('identify', function (name) {
      socket.set('name', String(name || 'Anonymous'), function (err) {
        updateRoster();
      });
    });
  });

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      broadcast('roster', names);
    }
  );
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
